use rocket::http::Status;
use rocket_dyn_templates::Template;
use serde::Serialize;

#[derive(Serialize)]
struct CountTemplate {
    s: fast_ec::queries::Statistics,
    project: String,
    seconds_elapsed: String,
}

#[derive(Serialize)]
struct ErrorTemplate {
    msg: String,
    seconds_elapsed: String,
}

#[rocket::get("/")]
async fn index() -> Template {
    Template::render("index", std::collections::HashMap::<&str, &str>::new())
}

#[rocket::get("/count?<project>&<username>")]
async fn count(
    project: &str,
    username: &str,
    cache: &rocket::State<sled::Tree>,
    credentials: &rocket::State<fast_ec::Credentials>,
) -> Result<Template, (Status, Template)> {
    let start = chrono::Utc::now();
    let statistics = fast_ec::go(project, username, cache, credentials).await;
    let seconds_elapsed = format!(
        "{:.2}",
        (chrono::Utc::now() - start).num_milliseconds() as f32 / 1000.0
    );
    Ok(match statistics {
        Ok(s) => Template::render(
            "count",
            CountTemplate {
                s,
                project: project.into(),
                seconds_elapsed,
            },
        ),
        Err(e) => Template::render(
            "error",
            ErrorTemplate {
                msg: e.into(),
                seconds_elapsed,
            },
        ),
    })
}

#[rocket::main]
async fn main() {
    let credentials_path =
        if std::env::var("FAST_EC_USE_LOCAL_CREDS").map_or(false, |val| !val.is_empty()) {
            "replica.my.cnf"
        } else {
            "/data/project/fast-ec/replica.my.cnf"
        };
    let credentials_file = ini::Ini::load_from_file(credentials_path).unwrap();
    let credentials = fast_ec::Credentials {
        username: credentials_file.section(Some("client")).unwrap().get("user").unwrap().to_string(),
        password: credentials_file.section(Some("client")).unwrap().get("password").unwrap().to_string(),
    };
    let _ = rocket::build()
        .mount("/", rocket::routes![index, count])
        .manage(sled::open("./database").unwrap().open_tree("data").unwrap())
        .manage(credentials)
        .attach(Template::fairing())
        .launch()
        .await
        .unwrap();
}
