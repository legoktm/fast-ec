pub mod db;
pub mod graphs;
pub mod queries;

pub struct Credentials {
    pub username: String,
    pub password: String,
}

pub async fn go_inner(
    pool: &mysql_async::Pool,
    project: &str,
    username: &str,
    cache: &sled::Tree,
) -> Result<queries::Statistics, String> {
    let Some((actor_id, user_id)) = queries::actor_id_and_user_id_from_username(&pool, username).await? else { return Err("No such user".into()) };
    let existing = db::fetch(cache, project, user_id);
    let existing_edits = existing.as_ref().map(|d| d.total_edits());
    let since = existing.as_ref().map(|d| d.data_as_of);
    let queries::QueriesResult {
        data: new,
        num_deleted_edits,
        optin,
    } = queries::go(actor_id, project, username, &pool, since).await;
    let Some(combined) = queries::combine_data(existing, new) else { return Err("No edits".into()) };
    db::put(cache, project, user_id, &combined);
    Ok(combined.to_statistics(username, optin, num_deleted_edits, since, existing_edits))
}

pub async fn go(
    project: &str,
    username: &str,
    cache: &sled::Tree,
    credentials: &Credentials,
) -> Result<queries::Statistics, String> {
    let pool = queries::get_pool(project, credentials);
    let res = go_inner(&pool, project, username, cache).await;
    pool.disconnect().await.unwrap();
    res
}
